#include "SocialNetwork.h"
#include <iostream>

void SocialNetwork::init(string networkName, int minAge) {
    name = networkName;
    min_age = minAge;
    p_list.init();
}

void SocialNetwork::clear() {
    p_list.clear();
}

string SocialNetwork::getNetworkName() const {
    return name;
}

int SocialNetwork::getMinAge() const {
    return min_age;
}

bool SocialNetwork::addProfile(Profile profile_to_add) {
    if (profile_to_add.getOwner().getAge() < min_age) {
        return false;
    }
    p_list.add(profile_to_add);
    return true;
}

string SocialNetwork::getWindowsDevices() const {
    string output;
    ProfileNode *itr = p_list.get_first();
    while (itr != nullptr) {
        DeviceNode *d_itr = itr->get_data().getOwner().getDevices().get_first(); // Devices list is always null. Fix needed.
        while (d_itr != nullptr) {
            if (isWindows(d_itr->get_data().getOS())) {
                if (!output.empty()) {
                    output += ",";
                }
                output += "[" + to_string(d_itr->get_data().getID()) + ", " + d_itr->get_data().getOS() + "]";
            }
            d_itr = d_itr->get_next();
        }
        itr = itr->get_next();
    }
    return output;
}

bool SocialNetwork::isWindows(string os) const{
    string check = "Windows";
    if(os.length() < check.length()){ // Not necessary but avoids the loop
        return false;
    }
    for(int i =0; i < 7; i++){
        if(os[i] != check[i]){
            return false;
        }
    }
    return true;
}