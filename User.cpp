#include "User.h"

void User::init(unsigned int id, std::string username, unsigned int age) {
    this->id = id;
    this->username = username;
    this->age = age;
    list.init();
}

void User::clear(){
    list.clear();
}

unsigned int User::getID() const {
    return id;
}

string User::getUserName() const {
    return username;
}

unsigned int User::getAge() const {
    return age;
}

DevicesList& User::getDevices(){
    return list;
}

void User::addDevice(Device newDevice){
    list.add(newDevice);
}
bool User::checkIfDevicesAreOn() const{
    bool res = true;
    DeviceNode* itr = list.get_first(); // Node we use to iterate over the list
    while(itr != nullptr){
        if(!itr->get_data().isActive()){
            res = false;
        }
        itr = itr->get_next();
    }
    return res;
}
