//
// Created by Daniel Zer kavod on 26/09/2023.
//

#ifndef SOCIAL_NETWORK_USER_H
#define SOCIAL_NETWORK_USER_H
#include <string>
#include "DeviceList.h"
using namespace std;

class User {
private:
    unsigned int id;
    string username;
    unsigned int age;
    DevicesList list;

public:
    void init(unsigned int id, string username, unsigned int age); // Constructor
    void clear(); // Destructor

    unsigned int getID() const;
    string getUserName() const;
    unsigned int getAge() const;
    DevicesList& getDevices();
    void addDevice(Device newDevice);
    bool checkIfDevicesAreOn() const;
};


#endif //SOCIAL_NETWORK_USER_H
