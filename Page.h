#ifndef SOCIAL_NETWORK_PAGE_H
#define SOCIAL_NETWORK_PAGE_H
#include <string>
using namespace std;

class Page {
private:
    string head_status;
    string posts;

public:
    void init();
    string getPosts() const;
    string getStatus() const;
    void clearPage();
    void setStatus(string status);
    void addLineToPosts(string new_line);
};


#endif //SOCIAL_NETWORK_PAGE_H
