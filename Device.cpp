//
// Created by Daniel Zer kavod on 24/09/2023.
//
#include "Device.h"
#include <iostream>

void Device::init(int id, DeviceType type, std::string os) {
    this->id = id;
    this->type = type;
    this->os = os;
    active = true;
}

unsigned int Device::getID() const{
    return id;
}

DeviceType Device::getType() const{
    return type;
}

string Device::getOS() const{
    return os;
}

bool Device::isActive() const{
    return active;
}

void Device::activate() {
    if (active) {
        cout << "Device is already active";
        return;
    }
    cout << "Device has been activated";
    active = true;
}

void Device::deactivate() {
    if (!active) {
        cout << "Device is already inactive" << endl;
        return;
    }
    cout << "Device has been deactivated" << endl;
    active = false;
}