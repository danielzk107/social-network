#pragma once

#include <string>

using namespace std;

///////////////////////////////////////////////////////////
/*														 //
	Those definitions are used for tests,                //
	you can add your own, but please do not delete any.  //
*/                                                         //
enum DeviceType {
    PHONE, PC, LAPTOP, TABLET
};             //
//
#define UbuntuOS "UbuntuLinux"                           //						
#define RedHatOS "RedHatLinux"                             //
#define MacOS "MacOS"                                     //
#define IOS "IOS"                                         //
#define WINDOWS7 "Windows7"                              //
#define WINDOWS10 "Windows10"                            //
#define WINDOWS11 "Windows11"                            //
#define ANDROID "Android"                                //
//
/// ///////////////////////////////////////////////////////


class Device {
private:
    int id;
    string os;
    DeviceType type;
    bool active;

public:
    void init(int id, DeviceType type, string os);

    unsigned int getID() const;

    DeviceType getType() const;

    string getOS() const;

    bool isActive() const;

    void activate();

    void deactivate();
};

