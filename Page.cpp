#include "Page.h"

void Page::init() {
    head_status = "";
    posts = "";
}
string Page::getPosts() const{
    return posts;
}
string Page::getStatus() const{
    return head_status;
}
void Page::clearPage() {
    posts = "";
}
void Page::setStatus(std::string status) {
    head_status = status;
}
void Page::addLineToPosts(std::string new_line) {
    posts += new_line + "\n";
}