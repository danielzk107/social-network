main: test1 test2 test3 Tester.o User.o Profile.o Page.o Device.o DeviceList.o UserList.o ProfileList.o SocialNetwork.o
	clang++ Tester.o User.o Profile.o Page.o Device.o DeviceList.o UserList.o ProfileList.o SocialNetwork.o -o main

#test: Main.o main
#	clang++ main.o User.o Profile.o Page.o Device.o DeviceList.o UserList.o ProfileList.o SocialNetwork.o -o tester

test1: test1Device.o Device.o
	clang++ test1Device.o Device.o -o test1

test2: test2User.o User.o DeviceList.o Device.o
	clang++ test2User.o User.o DeviceList.o Device.o -o test2

test3: test3Profile.o User.o Profile.o Page.o Device.o DeviceList.o UserList.o
	clang++ test3Profile.o User.o Profile.o Page.o Device.o DeviceList.o UserList.o -o test3

Main.o: Main.cpp
	clang++ -c Main.cpp

Device.o: Device.cpp
	clang++ -c Device.cpp

User.o: User.cpp
	clang++ -c User.cpp

Profile.o: Profile.cpp
	clang++ -c Profile.cpp

ProfileList.o: ProfileList.cpp
	clang++ -c ProfileList.cpp

SocialNetwork.o: SocialNetwork.cpp
	clang++ -c SocialNetwork.cpp

Page.o: Page.cpp
	clang++ -c Page.cpp

UserList.o: UserList.cpp
	clang++ -c UserList.cpp

test1Device.o: test1Device.cpp
	clang++ -c test1Device.cpp

test2User.o: test2User.cpp
	clang++ -c test2User.cpp

test3Profile.o: test3Profile.cpp
	clang++ -c test3Profile.cpp

Tester.o: Tester.cpp
	clang++ -c Tester.cpp

DeviceList.o: DeviceList.cpp
	clang++ -c DeviceList.cpp

clean:
	rm *.o main test1 test2 test3