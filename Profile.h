#ifndef SOCIAL_NETWORK_PROFILE_H
#define SOCIAL_NETWORK_PROFILE_H

#include "User.h"
#include "Page.h"
#include "UserList.h"
#include <string>

using namespace std;

class Profile {
private:
    User owner;
    Page page;
    UserList friends;

public:
    void init(User owner);

    void clear();

    User getOwner() const;

    void setStatus(string status);

    void addPostToProfilePage(std::string post);

    void addFriend(User friend_to_add);

    string getPage() const;

    string getFriends() const;

    string getFriendsWithSameNameLength() const;

    void changeAllWordsInStatus(string word);

    void changeWordInStatus(string word_to_replace,string new_word);
};


#endif //SOCIAL_NETWORK_PROFILE_H
