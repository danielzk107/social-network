#ifndef SOCIAL_NETWORK_SOCIALNETWORK_H
#define SOCIAL_NETWORK_SOCIALNETWORK_H

#include "ProfileList.h"
#include <string>
using namespace std;

class SocialNetwork {
private:
    string name;
    int min_age;
    ProfileList p_list;

public:
    void init(string networkName, int minAge);
    void clear();
    string getNetworkName() const;
    int getMinAge() const;
    bool addProfile(Profile profile_to_add);
    string getWindowsDevices() const;
    bool isWindows(string os) const;
};


#endif //SOCIAL_NETWORK_SOCIALNETWORK_H
