//
// Created by Daniel Zer kavod on 26/09/2023.
//
#include "Device.h"
#include "SocialNetwork.h"
#include <iostream>

int main(){
    Device dev;
    dev.init(1, PHONE, "Android");
    cout << "Testing getters:"<< endl;
    bool getters = true;
    if(dev.getID() == 1){
//        cout << "GetID is functioning correctly" << endl;
    }
    else{
        getters = false;
        cout << "GetID is functioning incorrectly" << endl;
    }
    if(dev.getOS() == "Android"){
//        cout << "getOS is functioning correctly" << endl;
    }
    else{
        getters = false;
        cout << "getOS is functioning incorrectly" << endl;
    }
    if(dev.getType() == PHONE){
//        cout << "getType is functioning correctly" << endl;
    }
    else{
        getters = false;
        cout << "getType is functioning incorrectly" << endl;
    }
    if(getters){
        cout << "All getters are functioning correctly" << endl;
    }
//    cout << "Device activation status: " << dev.isActive() << endl;
//    dev.deactivate();
//    cout << "Device activation status: " << dev.isActive() << endl;


    Device device1;
    device1.init(2123, LAPTOP, WINDOWS11);

    Device device2;
    device2.init(3212, PC, UbuntuOS);

    Device device3;
    device3.init(1121, TABLET, WINDOWS10);

    Device device4;
    device4.init(4134, PHONE, ANDROID);

    User user1;
    user1.init(123456789, "blinkybill", 17);
    User user2;
    user2.init(987654321, "HatichEshMiGilShesh", 15);
    user1.addDevice(device1);
    user1.addDevice(device2);
    cout << user1.getDevices().get_first() << endl << user1.getDevices().get_first()->get_next() << endl;
}
