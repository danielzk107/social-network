#include "Profile.h"
#include <iostream>
void Profile::init(User owner){
    this->owner = owner;
    page.init();
    friends.init();
}
void Profile::clear(){
//    owner.clear(); // Crashes tests; test 3 frees both users and profiles
    friends.clear();
}
User Profile::getOwner() const{
    User output;
    output.init(owner.getID(), owner.getUserName(), owner.getAge());
    return output;
}
void Profile::setStatus(string status){
    page.setStatus(status);
}
void Profile::addPostToProfilePage(std::string post){
    page.addLineToPosts(post);
}
void Profile::addFriend(User friend_to_add){
    friends.add(friend_to_add);
}
string Profile::getPage() const{
    string status = "Status: " + page.getStatus() + "\n*******************\n*******************\n";
    return status + page.getPosts();
}
string Profile::getFriends() const{
    string output = "";
    UserNode* itr = friends.get_first(); // Used to iterate over the friends list
    while(itr != nullptr){
        output += itr->get_data().getUserName();
        if(itr->get_next() != nullptr){
            output +=",";
        }
        itr = itr->get_next();
    }
    return output;
}
string Profile::getFriendsWithSameNameLength() const{
    // Same as previous function, with the added condition of the names being identical in length, as well as a small change in the comma placement check.
    string output = "";
    UserNode* itr = friends.get_first(); // Used to iterate over the friends list
    while(itr != nullptr){
        if(itr->get_data().getUserName().length() == owner.getUserName().length()){
            if(output != ""){
                output += ",";
            }
            output += itr->get_data().getUserName();
        }
        itr = itr->get_next();
    }
    return output;
}


void Profile::changeAllWordsInStatus(string word){}

void Profile::changeWordInStatus(string word_to_replace,string new_word){}